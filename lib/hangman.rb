require 'byebug'

class Player
  def register_secret_length(length = 0)
    @secret_word_length = length
  end

  def guess
    puts "Guess char"
    gets.chomp
  end

  def handle_response(board)
    puts board
  end
end

class Hangman
  DEFAULT_TRIES = 5
  attr_reader :guesser, :referee, :board
  def initialize(players, tries = DEFAULT_TRIES)
    @guesser = players[:guesser]
    @referee = players[:referee]
    raise "Error with given players" unless @guesser || @referee
    @board = []
    @tries = tries
  end

  def board
    @board.join
  end

  def setup
    secret_word_length = @referee.pick_secret_word
    @board = ["_"] * secret_word_length
    @guesser.register_secret_length(secret_word_length)
  end

  def take_turn
    guess = @guesser.guess
    matches = @referee.check_guess(guess)
    @tries -= 1 if matches.empty?
    update_board(matches, guess)

    @guesser.handle_response(board)
  end

  def play
    setup

    until tries == 0
      board

      take_turn
      if won?
        puts "Guessed secret word #{referee.secret_word}, #{tries} left."
        return
      end
    end
    puts "Game over!"
    puts "Secret word was #{referee.secret_word}"

  end

  def update_board(indices_match, guess)
    indices_match.each { |idx| @board[idx] = guess }
    board
  end

  private

  def won?
    @board.none? { |e| e == "_" }
  end

end

class HumanPlayer < Player
end

class ComputerPlayer < Player
  attr_reader :candidate_words, :secret_word
  DEFAULT_LOCATION = "dictionary.txt"

  def initialize(dictionary = nil)
    @dict = dictionary || load_dict
    @candidate_words = @dict
  end

  def pick_secret_word
    @secret_word = get_secret_word
    @secret_word.length
  end

  def check_guess(guess)
    indices = []
    @secret_word.chars.each_with_index do |elm, idx|
      indices << idx if elm.casecmp(guess) == 0
    end
    indices
  end

  def guess(board)
    get_best_guess(board)
  end

  def register_secret_length(length = 0)
    super(length)
    @candidate_words.select! { |word| word.length == length }
  end

  def handle_response(guess, indices)
    @candidate_words.select! do |word|
      select_word = true
      if indices.empty?
        select_word = !word.include?(guess)
      else
        select_word = word.count(guess) == indices.length
        if select_word
          indices.each do |idx|
            unless word[idx] == guess
              select_word = false
              break
            end
          end
          select_word
        end
      end
    end
  end

  private

  def get_best_guess(board)
    char_freq = get_freq(board)
    char_freq.sort_by { |k, v| v }.last[0]
  end

  def get_freq(board)
    char_freq = Hash.new(0)
    @candidate_words.each do |word|
      board.each_with_index do |char, idx|
        char_freq[word[idx]] += 1 if char.nil?
      end
    end
    char_freq
  end

  def load_dict(location = DEFAULT_LOCATION)
    dict_lines = []
    File.open(location) do |f|
      dict_lines << f.gets.chomp
    end
    dict_lines
  end

  def get_secret_word(length = nil)
    random = Random.new
    word_work_arr = length.nil? ? @dict : @dict.select { |word| word.length == length }
    word_work_arr[random.rand(word_work_arr.length)]
  end

end
